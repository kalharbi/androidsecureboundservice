# Android Secure Bound Service Example
This is an example on how to create and protect a bound service.
Android bound services allow application components to communicate with each other in a client-server fashion. A bound service is the server and the client can be any external component such as Activities or other apps' activities.

How to secure and protect a bound service?

To ensure your app is secure:

- Do not declare intent filters for the service.
- If intent filters are required for your service, protect it by a custom permission using the android:permission attribute, or set the package name for the intent with setPackage().
- ensure that your service is available to only your app by including the android:exported attribute and setting it to "false". 

See the AndroidManifest.xml file for more information.
