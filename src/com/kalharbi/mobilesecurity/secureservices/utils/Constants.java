package com.kalharbi.mobilesecurity.secureservices.utils;

public class Constants {
	public static final int PORT_STATUS_REQUEST = 100;
	public static final int PORT_STATUS_RESPONSE = 200;
	public static final String URL_KEY = "URL_KEY";
	public static final String PORT_KEY = "PORT_KEY";
	public static final String STATUS_KEY = "STATUS";
	
}
