package com.kalharbi.mobilesecurity.secureservices.activities;


import java.lang.ref.WeakReference;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.kalharbi.mobilesecurity.secureservices.R;
import com.kalharbi.mobilesecurity.secureservices.services.PortStatusService;
import com.kalharbi.mobilesecurity.secureservices.utils.Constants;

public class MainActivity extends Activity {
	private static final String TAG = MainActivity.class.getName();

	/** Messenger for communicating with the service. */
	private Messenger mMessenger = null;
	/** Flag indicating whether we have called bind on the service. */
	private boolean mBound;
	private TextView resultTextView;
	private EditText uriEditText, portEditText;
	private Button findBtn;
	
	/**
	 * Class for interacting with the main interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the object we can use to
			// interact with the service. We are communicating with the
			// service using a Messenger, so here we get a client-side
			// representation of that from the raw IBinder object.
			mMessenger = new Messenger(service);
			mBound = true;
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			mMessenger = null;
			mBound = false;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initViews();
		setClickListners();
	}

	private void initViews() {
		uriEditText = (EditText) this
				.findViewById(R.id.uriEditText);
		 portEditText = (EditText) this
				.findViewById(R.id.portEditText);

		findBtn = (Button) this
				.findViewById(R.id.getPortStatusBtn);

		resultTextView = (TextView) this.findViewById(R.id.textviewResult);
	}

	@Override
	protected void onStart() {
		super.onStart();
		// Bind to the service
		bindService(new Intent(this, PortStatusService.class), mConnection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		// Unbind from the service
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
	}

	private void setClickListners() {

		findBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Uri uri = Uri.parse(uriEditText.getText().toString());
				int port = Integer.parseInt(portEditText.getText().toString());
				getPortStatus(uri, port);
				// send intent to the registered broadcast receiver
				/*
				 * Intent intent = new Intent(); intent.setAction(
				 * "com.kalharbi.mobilesecurity.androidsecurebroadcast.broadcasts.PortReceiver"
				 * ); intent.putExtra("message", "Port 80 status");
				 * sendBroadcast(intent); Log.d(TAG, "broadcast sent");
				 */
				}
		});
	}

	// get port status from the bound service
	private void getPortStatus(Uri uri, int port) {
		/**
		 * Send message to the service.
		 */
		if (!mBound) {
			return;
		}
		// Create and send a message to the service, using a supported 'what'
		// value (PORT_STATUS_REQUEST)
		Message requestMSG = Message.obtain(null,
				Constants.PORT_STATUS_REQUEST, 0, 0);
		Bundle requestBundle = new Bundle();
		requestBundle.putString(Constants.URL_KEY, uri.toString());
		requestBundle.putInt(Constants.PORT_KEY, port);
		requestMSG.setData(requestBundle);
		
		requestMSG.replyTo = new Messenger(new ResponseHandler(this));
		try {
			mMessenger.send(requestMSG);
			Log.d(TAG, "Client sent a message to the service.");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	// This class handles the Service response
	static class ResponseHandler extends Handler {
		private final WeakReference<MainActivity> mActivity;

		public ResponseHandler(MainActivity activity) {
			this.mActivity = new WeakReference<MainActivity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			int respCode = msg.what;

			switch (respCode) {
			case Constants.PORT_STATUS_RESPONSE: {
				String result = msg.getData().getString(Constants.STATUS_KEY);
				mActivity.get().resultTextView.setText(result);
			}
			}
		}

	}
}
