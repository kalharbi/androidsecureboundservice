package com.kalharbi.mobilesecurity.secureservices;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import android.net.Uri;
import android.util.Log;

public class PortScanner implements Callable<Boolean> {
	private static final String TAG = PortScanner.class.getName();
	/**
	 * Use ExecutorService with Future in order to allow the background thread
	 * to return a result.
	 */

	private final ExecutorService executorService;
	private int portNumber;
	private Uri uri;

	public PortScanner(Uri uri, int portNumber) {
		this.uri = uri;
		this.portNumber = portNumber;
		this.executorService = Executors.newFixedThreadPool(1);
	}

	/**
	 * Check if this port is open or not.
	 * 
	 * @return true if the port is open and false if it is not open.
	 */
	public boolean isPortOpen() {
		// submit a task
		final Future<Boolean> future = executorService.submit(this);
		try {
			// wait and return the value of the method call()
			return future.get();
		} catch (InterruptedException e) {
			Log.e(TAG, e.getMessage());
		} catch (ExecutionException e) {
			Log.e(TAG, e.getMessage());
		}
		// shut down the thread pool executor.
		executorService.shutdown();
		return false;

	}

	// Check whether the port is open or not.
	@Override
	public Boolean call() throws Exception {
		Socket socket = new Socket();
		SocketAddress socketAddress = new InetSocketAddress(
				this.uri.toString(), this.portNumber);
		try {
			socket.connect(socketAddress, 1000);
			if (socket.isConnected()) {
				return true;
			}
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
		finally{
			socket.close();
		}
		
		return false;
	}
}
