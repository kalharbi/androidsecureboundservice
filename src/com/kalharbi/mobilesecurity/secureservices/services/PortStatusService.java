package com.kalharbi.mobilesecurity.secureservices.services;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.kalharbi.mobilesecurity.secureservices.PortScanner;
import com.kalharbi.mobilesecurity.secureservices.utils.Constants;

/**
 * A bound service that can be used to communicate with remote processes (using
 * Messenger).
 * 
 * @author khalid
 * 
 */
public class PortStatusService extends Service {
	private static final String TAG = PortStatusService.class.getName();
	// indicates whether onRebind should be used.
	private boolean mAllowRebind = true;

	/**
	 * An incoming handler of incoming messages from clients. This class is
	 * static to avoid potential memory leaks. If IncomingHandler class is not
	 * static, it will have a reference to the Service object. and the service
	 * object cannot be garbage collected, even after being destroyed.
	 */
	static class IncomingHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Constants.PORT_STATUS_REQUEST:
				// get incoming data
				String urlString = msg.getData().getString(Constants.URL_KEY);
				int portNumber = msg.getData().getInt(Constants.PORT_KEY);
				Uri uri = Uri.parse(urlString);
				// Check if the port is open
				PortScanner portScanner = new PortScanner(uri, portNumber);
				String result;
				if (portScanner.isPortOpen()) {
					result = "Port " + Integer.toString(portNumber)
							+ " is open.";
				} else {
					result = "Port is " + Integer.toString(portNumber)
							+ " closed.";
				}

				// Create the response message and prepare the response message.
				Bundle replyBundle = new Bundle();
				replyBundle.putString(Constants.STATUS_KEY, result);
				Message respMsg = Message.obtain(null,
						Constants.PORT_STATUS_RESPONSE);
				respMsg.setData(replyBundle);
				// Get the Messenger where replies to this message can be sent.
				Messenger replyMessenger = msg.replyTo;
				try {
					// Send a respond Message to the reply Messenger's Handler
					if (replyMessenger != null) {
						replyMessenger.send(respMsg);
					} else {
						Log.e(TAG, "Reply Messenger can not be found.");
					}

				} catch (RemoteException e) {
					Log.e(TAG, e.getMessage());
				}

				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	/**
	 * Target we publish for clients to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	/**
	 * Called when the service is being created.
	 */
	@Override
	public void onCreate() {
		Log.d(TAG, "the service has been created.");
	}

	/**
	 * A client is binding to the service with bindService() Called when a
	 * client is binding to the service. We return the communication channel to
	 * the service for sending messages to the service.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		Log.d(TAG, "A client has bound to the service.");
		// return the communication channel.
		return mMessenger.getBinder();
	}

	/**
	 * Called when all clients have unbound with unbindService()
	 */
	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(TAG, "All clients have unbound from the service.");
		return mAllowRebind;
	}

	/**
	 * A client is binding to the service with bindService() again, after
	 * onUnbind() has already been called
	 */
	@Override
	public void onRebind(Intent intent) {
		Log.d(TAG, "A client has rebound to the service.");
	}

	/**
	 * The system calls this method when the service is no longer used and is
	 * being destroyed. This is the last call the service receive.
	 */
	@Override
	public void onDestroy() {
		Log.d(TAG, "The service has been destoyed.");
	}
}
